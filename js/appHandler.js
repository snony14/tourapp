var application = angular.module('journeyApp', ['uiGmapgoogle-maps']);
var createMarker = function(marker_id, coordinates){
  var marker = {
    id: marker_id,
    coords: coordinates,
    options: { draggable: true }
  };
  return marker;
};

application.controller('mainCtrl', function ($scope, $log) {
    $scope.map = {center: {latitude: 59.329323, longitude: 18.068581} , zoom: 14 };
    $scope.options = {scrollwheel: true};
    $scope.totalMarkers = 0;
    $scope.markers = {};
    $scope.journeys = [];
    $scope.onMarkerClicked = function(id){

    };


  });

application.controller('buttonCtrl', function ($scope, $log){

  $scope.createMarker = function(){
    var id = (new Date).getTime();
    var offsetFromCenter = ($scope.totalMarkers + 0.0)/1000;
    var coord = {latitude: 59.329323 + offsetFromCenter, longitude: 18.068581 + offsetFromCenter};

    $scope.markers[id] = createMarker(id, coord);
    $scope.totalMarkers ++;
    return id;
  };

  $scope.createJourney = function()
  {
    if($scope.journeys == false){
      //initialise 
    }
  };
});
